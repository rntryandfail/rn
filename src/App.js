import React, {Component } from 'react';
import  {Cardlist} from './components/card-list/card-list.component';
import {SearchBox } from './components/search-box/search-box.component';
import './App.css';


class App extends Component {
  constructor() {
    super();

    this.state = {
      monsters: [],
      searchField: '',
    };

    //this.handleChange = this.handleChange.bind(this)
  }

   componentDidMount(){
     fetch('https://jsonplaceholder.typicode.com/users').
     then(response => response.json()).
     then(users => this.setState({monsters: users}))
   };

   handleChange = (e) => {
     this.setState({searchField: e.target.value})
   };

  render() { 
    const { monsters, searchField } = this.state;
    const filteredmonsters = monsters.filter(monster =>
      monster.name.toLowerCase().includes(searchField.toLowerCase()));
    return (
      <div className='App'> 
      {/* <input type='search' placeholder = 'Search for a monster' onChange = {e => 
        this.setState({searchField: e.target.value})
      }/> */}
      <h1>Monsters Rolodex</h1>
      <SearchBox handleChange = {this.handleChange} placeholder = 'Search for monsters'>

         </SearchBox>
        <Cardlist monsters = {filteredmonsters}>
        {/* {
            this.state.monsters.map(monster => (
            <h1 key = {monster.id}>{monster.name}</h1>
          ))
          } */}
        </Cardlist>
         
      </div>
    )
  }
}

export default App;
